        !COMPILER-GENERATED INTERFACE MODULE: Fri May 27 16:22:25 2016
        ! This source file is for reference only and may not completely
        ! represent the generated interface used by the compiler.
        MODULE INIT_COSMO_CONE__genmod
          INTERFACE 
            SUBROUTINE INIT_COSMO_CONE(OM0IN,OMLIN,HUBIN,OMEGA0,OMEGAL, &
     &OMEGAR,COVERH0)
              REAL(KIND=8) :: OM0IN
              REAL(KIND=8) :: OMLIN
              REAL(KIND=8) :: HUBIN
              REAL(KIND=8) :: OMEGA0
              REAL(KIND=8) :: OMEGAL
              REAL(KIND=8) :: OMEGAR
              REAL(KIND=8) :: COVERH0
            END SUBROUTINE INIT_COSMO_CONE
          END INTERFACE 
        END MODULE INIT_COSMO_CONE__genmod
