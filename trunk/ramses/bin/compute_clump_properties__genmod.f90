        !COMPILER-GENERATED INTERFACE MODULE: Fri May 27 16:23:04 2016
        ! This source file is for reference only and may not completely
        ! represent the generated interface used by the compiler.
        MODULE COMPUTE_CLUMP_PROPERTIES__genmod
          INTERFACE 
            SUBROUTINE COMPUTE_CLUMP_PROPERTIES(XX)
              USE AMR_COMMONS
              USE POISSON_COMMONS, ONLY :                               &
     &          PHI,                                                    &
     &          F
              REAL(KIND=8) :: XX(1:NCOARSE+NGRIDMAX*8)
            END SUBROUTINE COMPUTE_CLUMP_PROPERTIES
          END INTERFACE 
        END MODULE COMPUTE_CLUMP_PROPERTIES__genmod
