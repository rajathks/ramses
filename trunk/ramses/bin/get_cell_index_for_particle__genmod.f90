        !COMPILER-GENERATED INTERFACE MODULE: Fri May 27 16:22:57 2016
        ! This source file is for reference only and may not completely
        ! represent the generated interface used by the compiler.
        MODULE GET_CELL_INDEX_FOR_PARTICLE__genmod
          INTERFACE 
            SUBROUTINE GET_CELL_INDEX_FOR_PARTICLE(INDP,XX,CELL_LEV,    &
     &IND_GRID,XPART,IND_GRID_PART,NG,NP,ILEVEL,OK)
              INTEGER(KIND=4) :: INDP(1:32)
              REAL(KIND=8) :: XX(1:32,1:3)
              INTEGER(KIND=4) :: CELL_LEV(1:32)
              INTEGER(KIND=4) :: IND_GRID(1:32)
              REAL(KIND=8) :: XPART(1:32,1:3)
              INTEGER(KIND=4) :: IND_GRID_PART(1:32)
              INTEGER(KIND=4) :: NG
              INTEGER(KIND=4) :: NP
              INTEGER(KIND=4) :: ILEVEL
              LOGICAL(KIND=4) :: OK(1:32)
            END SUBROUTINE GET_CELL_INDEX_FOR_PARTICLE
          END INTERFACE 
        END MODULE GET_CELL_INDEX_FOR_PARTICLE__genmod
