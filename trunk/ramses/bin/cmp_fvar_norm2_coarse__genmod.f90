        !COMPILER-GENERATED INTERFACE MODULE: Fri May 27 16:23:16 2016
        ! This source file is for reference only and may not completely
        ! represent the generated interface used by the compiler.
        MODULE CMP_FVAR_NORM2_COARSE__genmod
          INTERFACE 
            SUBROUTINE CMP_FVAR_NORM2_COARSE(IVAR,ILEVEL,NORM2)
              INTEGER(KIND=4), INTENT(IN) :: IVAR
              INTEGER(KIND=4), INTENT(IN) :: ILEVEL
              REAL(KIND=8), INTENT(OUT) :: NORM2
            END SUBROUTINE CMP_FVAR_NORM2_COARSE
          END INTERFACE 
        END MODULE CMP_FVAR_NORM2_COARSE__genmod
