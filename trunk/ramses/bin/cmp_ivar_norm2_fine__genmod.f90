        !COMPILER-GENERATED INTERFACE MODULE: Fri May 27 16:23:14 2016
        ! This source file is for reference only and may not completely
        ! represent the generated interface used by the compiler.
        MODULE CMP_IVAR_NORM2_FINE__genmod
          INTERFACE 
            SUBROUTINE CMP_IVAR_NORM2_FINE(ILEVEL,IVAR,NORM2)
              INTEGER(KIND=4), INTENT(IN) :: ILEVEL
              INTEGER(KIND=4), INTENT(IN) :: IVAR
              REAL(KIND=8), INTENT(OUT) :: NORM2
            END SUBROUTINE CMP_IVAR_NORM2_FINE
          END INTERFACE 
        END MODULE CMP_IVAR_NORM2_FINE__genmod
