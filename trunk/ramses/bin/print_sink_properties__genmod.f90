        !COMPILER-GENERATED INTERFACE MODULE: Fri May 27 16:22:56 2016
        ! This source file is for reference only and may not completely
        ! represent the generated interface used by the compiler.
        MODULE PRINT_SINK_PROPERTIES__genmod
          INTERFACE 
            SUBROUTINE PRINT_SINK_PROPERTIES(DMEDOVERDT,RHO_INF,R2,     &
     &V_BONDI)
              USE PM_COMMONS
              REAL(KIND=8) :: DMEDOVERDT(1:NSINKMAX)
              REAL(KIND=8) :: RHO_INF
              REAL(KIND=8) :: R2
              REAL(KIND=8) :: V_BONDI
            END SUBROUTINE PRINT_SINK_PROPERTIES
          END INTERFACE 
        END MODULE PRINT_SINK_PROPERTIES__genmod
