        !COMPILER-GENERATED INTERFACE MODULE: Fri May 27 16:22:25 2016
        ! This source file is for reference only and may not completely
        ! represent the generated interface used by the compiler.
        MODULE POLINT__genmod
          INTERFACE 
            SUBROUTINE POLINT(XA,YA,N,X,Y,DY)
              INTEGER(KIND=4) :: N
              REAL(KIND=8) :: XA(N)
              REAL(KIND=8) :: YA(N)
              REAL(KIND=8) :: X
              REAL(KIND=8) :: Y
              REAL(KIND=8) :: DY
            END SUBROUTINE POLINT
          END INTERFACE 
        END MODULE POLINT__genmod
