        !COMPILER-GENERATED INTERFACE MODULE: Fri May 27 16:23:16 2016
        ! This source file is for reference only and may not completely
        ! represent the generated interface used by the compiler.
        MODULE GAUSS_SEIDEL_MG_COARSE__genmod
          INTERFACE 
            SUBROUTINE GAUSS_SEIDEL_MG_COARSE(ILEVEL,SAFE,REDSTEP)
              INTEGER(KIND=4), INTENT(IN) :: ILEVEL
              LOGICAL(KIND=4), INTENT(IN) :: SAFE
              LOGICAL(KIND=4), INTENT(IN) :: REDSTEP
            END SUBROUTINE GAUSS_SEIDEL_MG_COARSE
          END INTERFACE 
        END MODULE GAUSS_SEIDEL_MG_COARSE__genmod
