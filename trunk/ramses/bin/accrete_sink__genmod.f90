        !COMPILER-GENERATED INTERFACE MODULE: Fri May 27 16:22:57 2016
        ! This source file is for reference only and may not completely
        ! represent the generated interface used by the compiler.
        MODULE ACCRETE_SINK__genmod
          INTERFACE 
            SUBROUTINE ACCRETE_SINK(IND_GRID,IND_PART,IND_GRID_PART,NG, &
     &NP,ILEVEL,ON_CREATION)
              USE PM_COMMONS
              INTEGER(KIND=4) :: IND_GRID(1:32)
              INTEGER(KIND=4) :: IND_PART(1:32)
              INTEGER(KIND=4) :: IND_GRID_PART(1:32)
              INTEGER(KIND=4) :: NG
              INTEGER(KIND=4) :: NP
              INTEGER(KIND=4) :: ILEVEL
              LOGICAL(KIND=4) :: ON_CREATION
            END SUBROUTINE ACCRETE_SINK
          END INTERFACE 
        END MODULE ACCRETE_SINK__genmod
