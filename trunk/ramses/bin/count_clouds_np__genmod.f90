        !COMPILER-GENERATED INTERFACE MODULE: Fri May 27 16:22:58 2016
        ! This source file is for reference only and may not completely
        ! represent the generated interface used by the compiler.
        MODULE COUNT_CLOUDS_NP__genmod
          INTERFACE 
            SUBROUTINE COUNT_CLOUDS_NP(IND_GRID,IND_PART,IND_GRID_PART, &
     &NG,NP,ACTION,ILEVEL)
              INTEGER(KIND=4) :: IND_GRID(1:32)
              INTEGER(KIND=4) :: IND_PART(1:32)
              INTEGER(KIND=4) :: IND_GRID_PART(1:32)
              INTEGER(KIND=4) :: NG
              INTEGER(KIND=4) :: NP
              CHARACTER(LEN=15) :: ACTION
              INTEGER(KIND=4) :: ILEVEL
            END SUBROUTINE COUNT_CLOUDS_NP
          END INTERFACE 
        END MODULE COUNT_CLOUDS_NP__genmod
