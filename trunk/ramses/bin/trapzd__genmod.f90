        !COMPILER-GENERATED INTERFACE MODULE: Fri May 27 16:22:25 2016
        ! This source file is for reference only and may not completely
        ! represent the generated interface used by the compiler.
        MODULE TRAPZD__genmod
          INTERFACE 
            SUBROUTINE TRAPZD(A,B,S,N,OMEGA0,OMEGAL,OMEGAR)
              REAL(KIND=8) :: A
              REAL(KIND=8) :: B
              REAL(KIND=8) :: S
              INTEGER(KIND=4) :: N
              REAL(KIND=8) :: OMEGA0
              REAL(KIND=8) :: OMEGAL
              REAL(KIND=8) :: OMEGAR
            END SUBROUTINE TRAPZD
          END INTERFACE 
        END MODULE TRAPZD__genmod
