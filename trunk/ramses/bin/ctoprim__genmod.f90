        !COMPILER-GENERATED INTERFACE MODULE: Fri May 27 16:22:36 2016
        ! This source file is for reference only and may not completely
        ! represent the generated interface used by the compiler.
        MODULE CTOPRIM__genmod
          INTERFACE 
            SUBROUTINE CTOPRIM(UIN,Q,C,GRAVIN,DT,NGRID)
              REAL(KIND=8) :: UIN(1:32,-1:4,-1:4,-1:4,1:7)
              REAL(KIND=8) :: Q(1:32,-1:4,-1:4,-1:4,1:7)
              REAL(KIND=8) :: C(1:32,-1:4,-1:4,-1:4)
              REAL(KIND=8) :: GRAVIN(1:32,-1:4,-1:4,-1:4,1:3)
              REAL(KIND=8) :: DT
              INTEGER(KIND=4) :: NGRID
            END SUBROUTINE CTOPRIM
          END INTERFACE 
        END MODULE CTOPRIM__genmod
