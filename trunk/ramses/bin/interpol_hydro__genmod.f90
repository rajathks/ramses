        !COMPILER-GENERATED INTERFACE MODULE: Fri May 27 16:22:38 2016
        ! This source file is for reference only and may not completely
        ! represent the generated interface used by the compiler.
        MODULE INTERPOL_HYDRO__genmod
          INTERFACE 
            SUBROUTINE INTERPOL_HYDRO(U1,U2,NN)
              REAL(KIND=8) :: U1(1:32,0:6,1:7)
              REAL(KIND=8) :: U2(1:32,1:8,1:7)
              INTEGER(KIND=4) :: NN
            END SUBROUTINE INTERPOL_HYDRO
          END INTERFACE 
        END MODULE INTERPOL_HYDRO__genmod
