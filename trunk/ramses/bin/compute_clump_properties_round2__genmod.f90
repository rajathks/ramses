        !COMPILER-GENERATED INTERFACE MODULE: Fri May 27 16:23:05 2016
        ! This source file is for reference only and may not completely
        ! represent the generated interface used by the compiler.
        MODULE COMPUTE_CLUMP_PROPERTIES_ROUND2__genmod
          INTERFACE 
            SUBROUTINE COMPUTE_CLUMP_PROPERTIES_ROUND2(XX)
              USE AMR_COMMONS
              USE PM_COMMONS, ONLY :                                    &
     &          CONT_SPEED
              USE CLFIND_COMMONS
              REAL(KIND=8) :: XX(1:NCOARSE+NGRIDMAX*8)
            END SUBROUTINE COMPUTE_CLUMP_PROPERTIES_ROUND2
          END INTERFACE 
        END MODULE COMPUTE_CLUMP_PROPERTIES_ROUND2__genmod
