        !COMPILER-GENERATED INTERFACE MODULE: Fri May 27 16:22:25 2016
        ! This source file is for reference only and may not completely
        ! represent the generated interface used by the compiler.
        MODULE COMPUTE_MINIMUM_POLYGON__genmod
          INTERFACE 
            SUBROUTINE COMPUTE_MINIMUM_POLYGON(X1,X2,THETAYRAD,THETAZRAD&
     &,SL)
              REAL(KIND=8) :: X1
              REAL(KIND=8) :: X2
              REAL(KIND=8) :: THETAYRAD
              REAL(KIND=8) :: THETAZRAD
              REAL(KIND=8) :: SL(3,8)
            END SUBROUTINE COMPUTE_MINIMUM_POLYGON
          END INTERFACE 
        END MODULE COMPUTE_MINIMUM_POLYGON__genmod
