        !COMPILER-GENERATED INTERFACE MODULE: Fri May 27 16:23:05 2016
        ! This source file is for reference only and may not completely
        ! represent the generated interface used by the compiler.
        MODULE JACOBI__genmod
          INTERFACE 
            SUBROUTINE JACOBI(A,X,ERR2)
              REAL(KIND=8) :: A(3,3)
              REAL(KIND=8) :: X(3,3)
              REAL(KIND=8) :: ERR2
            END SUBROUTINE JACOBI
          END INTERFACE 
        END MODULE JACOBI__genmod
