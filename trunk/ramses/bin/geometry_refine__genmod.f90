        !COMPILER-GENERATED INTERFACE MODULE: Fri May 27 16:22:13 2016
        ! This source file is for reference only and may not completely
        ! represent the generated interface used by the compiler.
        MODULE GEOMETRY_REFINE__genmod
          INTERFACE 
            SUBROUTINE GEOMETRY_REFINE(XX,IND_CELL,OK,NCELL,ILEVEL)
              REAL(KIND=8) :: XX(1:32,1:3)
              INTEGER(KIND=4) :: IND_CELL(1:32)
              LOGICAL(KIND=4) :: OK(1:32)
              INTEGER(KIND=4) :: NCELL
              INTEGER(KIND=4) :: ILEVEL
            END SUBROUTINE GEOMETRY_REFINE
          END INTERFACE 
        END MODULE GEOMETRY_REFINE__genmod
