        !COMPILER-GENERATED INTERFACE MODULE: Fri May 27 16:22:25 2016
        ! This source file is for reference only and may not completely
        ! represent the generated interface used by the compiler.
        MODULE COORD_DISTANCE__genmod
          INTERFACE 
            FUNCTION COORD_DISTANCE(ZZ,OMEGA0,OMEGAL,OMEGAR,COVERH0)
              REAL(KIND=8) :: ZZ
              REAL(KIND=8) :: OMEGA0
              REAL(KIND=8) :: OMEGAL
              REAL(KIND=8) :: OMEGAR
              REAL(KIND=8) :: COVERH0
              REAL(KIND=8) :: COORD_DISTANCE
            END FUNCTION COORD_DISTANCE
          END INTERFACE 
        END MODULE COORD_DISTANCE__genmod
