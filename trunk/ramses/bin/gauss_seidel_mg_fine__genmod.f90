        !COMPILER-GENERATED INTERFACE MODULE: Fri May 27 16:23:14 2016
        ! This source file is for reference only and may not completely
        ! represent the generated interface used by the compiler.
        MODULE GAUSS_SEIDEL_MG_FINE__genmod
          INTERFACE 
            SUBROUTINE GAUSS_SEIDEL_MG_FINE(ILEVEL,REDSTEP)
              INTEGER(KIND=4), INTENT(IN) :: ILEVEL
              LOGICAL(KIND=4), INTENT(IN) :: REDSTEP
            END SUBROUTINE GAUSS_SEIDEL_MG_FINE
          END INTERFACE 
        END MODULE GAUSS_SEIDEL_MG_FINE__genmod
