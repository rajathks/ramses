        !COMPILER-GENERATED INTERFACE MODULE: Fri May 27 16:22:15 2016
        ! This source file is for reference only and may not completely
        ! represent the generated interface used by the compiler.
        MODULE MAKE_VIRTUAL_COARSE_INT__genmod
          INTERFACE 
            SUBROUTINE MAKE_VIRTUAL_COARSE_INT(XX)
              USE AMR_COMMONS
              INTEGER(KIND=4) :: XX(1:NCOARSE+NGRIDMAX*8)
            END SUBROUTINE MAKE_VIRTUAL_COARSE_INT
          END INTERFACE 
        END MODULE MAKE_VIRTUAL_COARSE_INT__genmod
